#!/usr/bin/env node
/*
 *  Proof of Concept for complex forms with HAM
 *  Copyright (C) 2021-2022 Sascha Kohlmann
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.      
 */

/*
 * This is the server part of the PoC.
 *
 * NOTE: There is no real sepration between the API, the Domain model and
 *       the infrastructure as this is only a proof of concept. There is
 *       no idea to make this PoC a product and it should never be used as
 *       a product.
 * 
 *       It's also a private project to learn server side Javascript.
 *       And Javascript is a pain. A real hard pain. That's not fun at all.
 * 
 * Learnings
 * =========
 * 
 * 1. Validation of values can be complex in case. Suppose the following situation: a sick leave because of an
 *    ill child.
 *
 *    Birthday of the child : 2021-12-28
 *    Sick leave start day  : 2021-12-26
 *    Sick leave finish day : 2021-12-27
 *
 *    This is obivous not possible. There must be a rule, that the "Sick leave start day" can't be before
 *    "Birthday of the child" and the "Sick leave finish day" also can't be before the "Birthday of the child".
 *
 *    This means also: based on the rules of the input validation, there is an implicit order of the
 *    availability of the data.
 *
 * 2. For Dates there must also be a rule that there is a maximum duration between start and end date. 
 *    If start and end date are swapped, means start date is after end date it's the responsebility of the API
 *    to correct this tacitly.
 *
 * 3. The start date and the end date should also be starting by a calculatable date, e.g. from today() on.
 *    The rule engine must gurantee that the start date is not after the end date.
 *
 * 4. The learnings 1., 2. and 3. also for time and datetime.
 *
 *    As always: time is the most complicated task 🧐
 *
 *    May be best is to offer in a more generalized form system a "range" type. Not even for date, time and
 *    datetime but also for numerical values.
 *
 * 5. Date and time handling APIs should require a HTTP Date header (RFC 7231, 7.1.1.2) if HTML <input> elements
 *    of type "date", "time" and/or "datetime-local" (https://html.spec.whatwg.org/multipage/input.html#attr-input-type)
 *    and the "min" and/or "max" attributes are set to current date or time.
 * 
 * 6. If there is a process with multiple steps, how to show the client that the final step is yet not done?
 *    The state data is part of the HAM document. No state on server side.
 *    So multiple steps means: not all required data in the state.
 *    The state contains the entity which means, we can use HTTP Status 422 (Unprocessable Entity).
 *    Even though it is a WebDAV code, it can still be used. It is also part of "HTTP Semantics draft-ietf-httpbis-semantics-19".
 *    Additional: see also https://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml
 * 
 *    Other failures should be shown with a 400 (Bad Request) even if the syntax is correct.
 */
"use strict"
const fs = require('fs');
const { request } = require('http');
const { getSystemErrorMap } = require('util');

const apiPrefix = '/api';

// See also https://www.fastify.io
// npm install --save fastify
// npm install --save fastify-formbody
// npm install --save fastify-accepts  // https://github.com/fastify/fastify-accepts
// For QA:
// npm install --save-dev jest
const server = require('fastify')({
    ignoreTrailingSlash: true,
    logger: true
});
server.register(require('fastify-formbody'));
server.register(require('fastify-accepts'));
const HOST = process.env.HOST || '127.0.0.1';
const PORT = process.env.PORT || 1337;

const EMPTY_ARRAY = [];

server.get(apiPrefix + '/forms', async (request, reply) => {

    reply.header('Content-Type', 'application/vnd.ham+json');

    let ham = baseHam();
    addGlobalLinks(ham, {rel:'self', href: apiUrl(request) + "/forms" });

    let item1 = {};
    item1.id = newId();
    item1.links = [];
    item1.links.push({rel: 'create-form', href: apiUrl(request) + '/forms/KIKG-corona', prompt: 'anmelden'});
    item1.links.push({rel: 'about', href: 'https://www.gesetze-im-internet.de/sgb_5/__45.html', prompt: 'Gesetzliche Grundlage'});
    item1.entries = [];
    item1.entries.push({name: 'description', value: 'Receipt of child sickness benefit in the event of pandemic care of the child who is not ill.', prompt: 'Beschreibung', id: newId()});
    item1.entries.push({name: 'legal', value: ['§ 45 Abs. 2a SGB V', 'second'], prompt: 'Gesetzliche Grundlage'});
    ham.items.push(item1);

    return toBase(cleanedHam(ham));
})

server.get(apiPrefix + '/forms/:id', async (request, reply) => {
    reply.header('Content-Type', 'application/vnd.ham+json');

    var ham = baseHam();
    if (request.params['id'] == "KIKG-corona") {
        ham = createKIKGCoronaOn(request, ham);
    } else {
        reply.code(404);
        ham = addNoSuchFormProblem(ham, request.params['id']);
    }

    return toBase(cleanedHam(ham));
})

server.put(apiPrefix + '/forms/:id', async (request, reply) => {
    reply.header('Content-Type', 'application/vnd.ham+json');

    let state = new KIKGcorona(request.body);
    state.problems();

    var ham = baseHam();
    addGlobalLinks(ham, {rel:'prev', href: apiUrl(request) + "/forms", prompt: 'Zurück'});
    state.items().forEach(item => ham.items.push(item));
    state.problems().forEach(problem => ham.problems.push(problem));

    if (state.status() >= 300) {
        ham.items.push(state.dataItemForStep(state.step));
        ham.templates['default'] = state.nextStateTemplate(apiUrl(request) + "/forms/" + request.params['id'], "PUT");
    }

    return toBase(cleanedHam(ham));
})

function createKIKGCoronaOn(request, ham) {
    if (!request) { return ham; }
    var ham = baseHam(ham);

    ham = addGlobalLinks(ham, {rel: 'prev', href: apiUrl(request) + "/forms", prompt: "Zurück zur Formularliste" });

    let kikg = new KIKGcorona(null);

    let kikgItems = kikg.items();
    kikgItems.forEach(item => ham.items.push(item));
    let template = kikg.nextStateTemplate(apiUrl(request) + "/forms/" + request.params['id'], "PUT");

    if (template) {
        if (!ham.templates) {
            ham.templates = {};
        }
        ham.templates['default'] = template;
    }

    return ham;
}

function templateBase(templateBaseData) {
    if (!templateBaseData) {
        throw 'template base data expected';
    }

    if (!isString(templateBaseData.href)) {
        throw 'href is not optional and must be a string: ' + templateBaseData.href;
    }

    let template = {};
    template.params = [];

    template.href = templateBaseData.href;
    if (templateBaseData.rel) {
        template.rel = templateBaseData.rel;
    }
    template.method = httpVerb(templateBaseData.method);
    if (isString(templateBaseData.model)) {
        template.model = templateBaseData.model;
    } else {
        template.model = 'application/x-www-form-urlencoded';
    }
    if (isString(templateBaseData.prompt)) {
        template.prompt = templateBaseData.prompt;
    }
    return template;
}

function templateName(name) {
    if (!name) {
        return "default";
    }
    if (!isString(name)) {
        throw "Template name is not optiona and must be of type String";
    }

    return name;
}

function addNoSuchFormProblem(ham, id) {
    var ham = baseHam(ham);
    ham = addGlobalLinks(ham, {rel: 'prev', href: apiUrl(request) + "/forms", prompt: "Zurück zur Formularliste" });
    ham = addProblem(ham, {category: "http://example.org/problem/nosuchform", title: "Unbekanntes Formular", detail: "Das Formular mit Kennung '" + id + "' ist nicht bekannt. Bitte versuchen Sie es erneut."});
    return ham;
}

function addProblem(ham, problemData) {
    if (!problemData) {
        return ham;
    }
    ham = baseHam(ham);
    let problem = {};

    if (isString(problemData.category)) {
        problem.category = problemData.category;
    }
    if (isString(problemData.title)) {
        problem.title = problemData.title;
    }
    if (isString(problemData.detail)) {
        problem.detail = problemData.detail;
    }
    if (containsOnlyString(problemData.idrefs)) {
        problem.idrefs = problemData.idrefs;
    }
    ham.problems.push(problem);

    return ham;
}

function containsOnlyString(array) {
    if (isArray(array) && isNotEmptyArray(array)) {
        for (let i = 0; i < array.length; i++) {
            if (!isString(array[i])) {
                return false;
            }
        }
        return true;
    }
    return false;
}

function httpVerb(verb) {
    if (!verb) {
        return "GET";
    }
    if (isSupportedHttpVerb(verb)) {
        return verb.toUpperCase();
    }

    throw "Unsupported HTTP verb: " + verb;
}

function isSupportedHttpVerb(verb) {
    if (isString(verb)) {
        let uppercase = verb.toUpperCase();
        return uppercase === "PUT" || uppercase === "POST" || uppercase === "GET";
    }

    return false;
}

function isString(obj) {
    return typeof obj === 'string' || obj instanceof String;
}

function isBoolean(obj) {
    return typeof obj === 'boolean' || obj instanceof Boolean;
}

function isArray(obj) {
    return Array.isArray(obj);
}

function isNotEmptyArray(obj) {
    if (isArray(obj)) {
        return obj.length != 0;
    }
    return false;
}

function addGlobalLinks(ham, linkdata) {

    if (!linkdata) {
        return ham;
    }

    if (!isString(linkdata.href) || !isString(linkdata.rel)) {
        throw 'href and rel are not optional and must be of type string';
    }

    ham = baseHam(ham);

    let link = {};
    link.href = linkdata.href;
    link.rel = linkdata.rel;

    if (isString(linkdata.id)) {
        link.id = linkdata.id;
    }
    if (isString(linkdata.name)) {
        link.name = linkdata.name;
    }
    if (isString(linkdata.prompt)) {
        link.prompt = linkdata.prompt;
    }

    if (isBoolean(linkdata.transclude)) {
        link.transclude = linkdata.transclude;
    }
    ham.links.push(link);

    return ham;
}

function baseHam(ham) {
    if (ham) {
        return ham;
    }
    ham = {};
    ham.schema = "Draft-1";
    ham.links = [];
    ham.items = [];
    ham.templates = {};
    ham.problems = [];
    return ham;
}

function cleanedHam(ham) {
    if (!ham) { return; }
    let retHam = {};
    retHam.schema = ham.schema;
    if (ham.links && ham.links.length !== 0) {
        retHam.links = ham.links;
    }
    if (ham.items && ham.items.length !== 0) {
        retHam.items = ham.items;
    }
    if (ham.templates && Object.keys(ham.templates).length !== 0) {
        retHam.templates = ham.templates;
    }
    if (ham.problems && ham.problems.length !== 0) {
        retHam.problems = ham.problems;
    }

    return retHam;
}

function toBase(ham) {
    let base = {};
    base.ham = ham;
    console.log(JSON.stringify(base, null, "  "));
    return base;
}

server.get('/', async (request, reply) => {
    const stream = fs.createReadStream(__dirname + '/index.html', 'utf8');
    reply.header('Content-Type', 'text/html');
    reply.send(stream);
    console.log(request.headers);
})

server.get('/:file', async (request, reply) => {
    let filename = request.params['file'];
    const stream = fs.createReadStream(__dirname + '/' + filename, 'utf8');
    if (filename.endsWith("js")) {
        reply.header('Content-Type', 'application/javascript');
    } else if (filename.endsWith("css")) {
        reply.header('Content-Type', 'text/css');
    }
    reply.send(stream);
})

function apiUrl(request) {
    return baseUrl(request) + apiPrefix;
}

function baseUrl(request) {
    return request.protocol + '://' + request.hostname;
}

server.listen(PORT, HOST, () => {
    console.log(`Server running at http://${HOST}:${PORT}`);
})

function newId() {
    return 'hamham' + Math.floor(Math.random() * 1_000_000_000);
}

const YEAR = 1;
class DateSupport {

    static minus(date, value, type) {
        let copy = structuredClone(date);
        if (type === YEAR) {
            copy.setFullYear(date.getFullYear() - value);
        } else {
            throw "Unsupported minus type: " + type;
        }
        return copy;
    }

    static plus(date, value, type) {
        let copy = { ...date };
        if (type === YEAR) {
            copy.setFullYear(date.getFullYear() + value);
        } else if (type === MONTH) {
            copy.setMonth(date.getMonth() + value);
        } else if (type === DAY) {
            copy.setDate(date.getDay() + value);
        } else {
            throw "Unsupported minus type: " + type;
        }
        return copy;
    }

    static toIsoDate(date = new Date()) {
        var iso = date.toISOString();
        let firstT = iso.indexOf("T");
        return iso.substr(0, firstT);
    }
}


class KIKGcorona {

    kikgCoronaStepParams = new Steps([new ParamType("_version", {data: "1", type: "hidden"})]);
    nextStep = "start";
    previousSteps = [];
    currentProblems = null;

    constructor(state) {
        this.state = state;
        this.angabenZumKindId = newId();
        this.childLastnameId = newId();
        this.childBirthDateId = newId();
        this.childHealthInsurance = newId();
        this.zeitraumBetreuungStartId = newId();
        this.zeitraumBetreuungEndId = newId();

        // This is poor design, I know 😜
        this.kikgCoronaStepParams = new Steps([new ParamType("_version", {data: "1", type: "hidden"})]);
        this.kikgCoronaStepParams.addParamTypesForStep("start", [
            new ParamType("child-firstname", {type: "text", prompt: "Vorname des Kindes", placeholder: "Vorname", class: ["group+angaben_zum_kind"], id: this.angabenZumKindId, constraints: new Constraints({maxItems: 1, required: true, min: "1", max: "100"})}),
            new ParamType("child-lastname", {type: "text", prompt: "Nachname des Kindes", placeholder: "Nachname", class: ["group+angaben_zum_kind"], id: this.childLastnameId, constraints: new Constraints({maxItems: 1, required: true, min: "1", max: "100"}) }),
            new ParamType("child-birthdate", {type: "date", prompt: "Geburtsdatum", class: ["group+angaben_zum_kind"], id: this.childBirthDateId, constraints: new Constraints({maxItems: 1, max: DateSupport.toIsoDate(), min: DateSupport.toIsoDate(DateSupport.minus(new Date(), 12, YEAR)), required: true}) }),
            new ParamType("child-healthinsurance", {type: "search", prompt: "Krankenkasse", class: ["group+angaben_zum_kind"], id: this.childHealthInsurance, placeholder: "Name der Krankenkasse des Kindes", constraints: new Constraints({maxItems: 1, max: "100"}), selectables: selectablesForKrankenkassen() })
        ]);
        this.kikgCoronaStepParams.addCommonItemsForStep("start", [
            new Item().withLinks([new Link("type", "http://example.org/help", {prompt: "Hilfe"}),
                                  new Link("describes", "#" + this.angabenZumKindId, {prompt: "Describes", name: "group+angaben_zum_kind"})
                ]).withEntries([new Entry("group-description", {value: "Angaben zum Kind"})
                ])
        ]);

        this.kikgCoronaStepParams.addParamTypesForStep("zeitraum", [
            new ParamType("zeitraum-start", {type: "date", prompt: "Von", class: ["group+zeitraum"], id: this.zeitraumBetreuungStartId, constraints: new Constraints({maxItems: 1, required: true, max: DateSupport.toIsoDate()})}),
            new ParamType("zeitraum-end", {type: "date", prompt: "Bis (einschliesslich)", class: ["group+zeitraum"], id: this.zeitraumBetreuungEndId, constraints: new Constraints({maxItems: 1, required: true, max: DateSupport.toIsoDate()})})
        ]);
        this.kikgCoronaStepParams.addCommonItemsForStep("zeitraum", [
            new Item().withLinks([new Link("type", "http://example.org/help", {prompt: "Hilfe"}),
                                  new Link("describes", "#" + this.zeitraumBetreuungStartId, {prompt: "Describes", name: "group+zeitrum"})
                ]).withEntries([new Entry("group-description", {value: "Angaben zum Zeitraum der Betreuung"})
                ])
        ]);
    }

    /**
     * @param {*} href The href for the template
     * @returns a HAM template without an object name
     */
    nextStateTemplate(href, method) {
        if (!href) { throw "Missing href value" }
        if (!this.state) {
            let template = templateBase({href: href,
                                        method: method,
                                        rel: "next",
                                        prompt: "Receipt of child sickness benefit in the event of pandemic care of the child who is not ill"});

            this.kikgCoronaStepParams.paramTypesForStep(this.nextStep).forEach(paramType => template.params.push(paramType.toHamParam()));
            return template;
        } else {
            if (this.currentProblems && this.currentProblems.length !== 0 && this.nextStep === "start") {
                console.log("Repeat 'start' because of problems");
                let template = templateBase({href: href,
                    method: method,
                    rel: "next",
                    prompt: "Receipt of child sickness benefit in the event of pandemic care of the child who is not ill"});
                this.kikgCoronaStepParams.paramTypesForStep("start").forEach(paramType => template.params.push(paramType.toHamParam({data: this.valueForParam(paramType.name)})));
                return template;
            } else if (!this.currentProblems && this.nextStep === "zeitraum") {
                console.log("Go with 'zeitraum'");
                let template = templateBase({href: href,
                    method: method,
                    rel: "next",
                    prompt: "Period of care"});
                this.kikgCoronaStepParams.paramTypesForStep("start").forEach(paramType => template.params.push(paramType.toHiddenHamParam(this.valueForParam(paramType.name))));
                this.kikgCoronaStepParams.paramTypesForStep("zeitraum", false).forEach(paramType => template.params.push(paramType.toHamParam()));
                return template;
            }
        }
    }   

    valueForParam(paramName) {
        if (!paramName) { return }
        if (!this.state) { return }
        return this.state[paramName];
    }

    problems() {
        if (!this.state) { return; }
        let problems = [];
        if (this.state["_current-step"] == "start") {
            this.checkStep1(problems);
        }
        if (problems.length !== 0) {
            this.currentProblems = problems;
        } else {
            this.nextStep = "zeitraum";
        }
        return problems;
    }

    /**
     * @returns A HAM items array
     */
    items() {
        let items = [];
        this.kikgCoronaStepParams.commonItemsForStep(this.nextStep).forEach(item => items.push(item.toHamItem()));
        let hamDataItem = this.dataItemForStep(this.nextStep);
        if (hamDataItem.containsLinksOrEntries()) {
            items.push(hamDataItem.toHamItem());
        }
        return items;
    }

    dataItemForStep(stepName) {
        var item = new Item();
        if (this.state) {
            let paramTypes = this.kikgCoronaStepParams.paramTypesForStep(stepName);
            paramTypes.forEach(paramType => {
                let paramTypeName = paramType.name;
                let paramTypeStepValue = this.state[paramTypeName];
                if (paramTypeStepValue) {
                    item = item.addEntries([new Entry(paramTypeName, {value: paramTypeStepValue, prompt: paramType.prompt ?? paramTypeName})]);
                }
            });
        }   

        return item;
    }

    checkStep1(problems) {
        if (!this.state["child-firstname"] || this.state["child-firstname"].length < 1) {
            problems.push(ProblemFactory.missing("Vorname des Kindes nicht angegeben", this.angabenZumKindId));
        } else {
            if (this.state["child-firstname"].length > 100) {
                problems.push(ProblemFactory.tooLong("Vorname des Kindes zu lang (" + this.state["child-firstname"].length + " Zeichen). Erlaubt sind maximal 100 Zeichen.", this.angabenZumKindId));
            }
        }
        if (!this.state["child-lastname"] || this.state["child-lastname"].length < 1) {
            problems.push(ProblemFactory.missing("Nachname des Kindes nicht angegeben", this.angabenZumKindId));
        } else {
            if (this.state["child-lastname"].length > 100) {
                problems.push(ProblemFactory.tooLong("Nachname des Kindes zu lang (" + this.state["child-lastname"].length + " Zeichen). Erlaubt sind maximal 100 Zeichen.", this.angabenZumKindId));
            }
        }
        if (!this.state["child-birthdate"]) {
            problems.push(ProblemFactory.missing("Geburtsdatum muss angegeben sein.", this.angabenZumKindId));
        } else {
            if (this.state["child-birthdate"] && new Date(this.state["child-birthdate"]) > new Date()) {
                problems.push(ProblemFactory.dateInFuture("Geburtsdatum kann nicht in Zukunft sein.", this.angabenZumKindId));
            }
            if (this.state["child-birthdate"] && new Date(this.state["child-birthdate"]) < DateSupport.minus(new Date(), 12, YEAR)) {
                problems.push(ProblemFactory.dateToLate("Das Kind ist zu alt. Kinder über 12 Jahren werden nicht mehr berücksichtigt.", this.angabenZumKindId));
            }
        }
    }

    status() {
        return 422;
    }
}

class ProblemFactory {
    static missing(detail, targetId) {
        return {category: "http://example.org/problem/missing", title: "Missing data", detail: detail, idrefs: [targetId] };
    }
    static tooLong(detail, targetId) {
        return {category: "http://example.org/problem/toolong", title: "Size out of range", detail: detail, idrefs: [targetId] };
    }
    static dateInFuture(detail, targetId) {
        return {category: "http://example.org/problem/future", title: "Date in future", detail: detail, idrefs: [targetId] };
    }
    static dateToLate(detail, targetId) {
        return {category: "http://example.org/problem/toolate", title: "Date to late", detail: detail, idrefs: [targetId] };
    }
}

class ParamType {
    constructor(name, additional = {type : "text", data: "", prompt : "", placeholder : "", class: [], constraints : null, selectables : []}) {
        if (!name || name.length === 0) {
            throw "Name must be provided";
        }
        this.name = name;
        this.type = additional?.type ?? "text";
        this.data = additional?.data ?? "";
        this.prompt = additional?.prompt ?? "";
        this.placeholder = additional?.placeholder ?? "";
        this.class = additional?.class ?? [];
        this.constraints = additional?.constraints;
        this.selectables = additional?.selectables ?? [];
    }

    clone() {
        return new ParamType(this.name, {type: this.type, data: this.data, prompt: this.prompt, placeholder: this.placeholder, class: [...this.class], constraints: this.constraints, selectables: Selectable.cloneArray(this.selectables)});
    }

    withType(type = "") {
        let newParamType = this.clone();
        newParamType.type = type ?? "text";
        return newParamType;
    }
    /**
     * Data is reuired e.g. for "checkbox" or "radio" types.
     * @param {*} data the default data
     * @returns new instance of ParamType
     */
    withData(data = "") {
        let newParamType = this.clone();
        newParamType.data = data ?? "";
        return newParamType;
    }
    withPrompt(prompt = "") {
        let newParamType = this.clone();
        newParamType.prompt = prompt ?? "";
        return newParamType;
    }
    withPlaceholder(placeholder = "") {
        let newParamType = this.clone();
        newParamType.placeholder = placeholder ?? "";
        return newParamType;
    }
    withClasses(classes = []) {
        let newParamType = this.clone();
        newParamType.class = classes ?? [];
        return newParamType;
    }
    withClass(clazz = []) {
        if (Array.isArray(clazz)) {
            return this.withClasses(clazz)
        }
        return this.withClasses([clazz]) // Strange: 'this' is required in this context. Otherwise a ReferenceError is thrown
    }
    withConstraints(constraints) {
        let newParamType = this.clone();
        newParamType.constraints = constraints;
        return newParamType;
    }
    withSelectables(selectables = []) {
        let newParamType = this.clone();
        newParamType.selectables = selectables ?? [];
        return newParamType;
    }

    toHamParam(values = {data : "", id : ""}) {
        let param = {name: this.name, type: this.type};

        if (values?.data && typeof values?.data === 'string' && values?.data.length !== 0) {
            param.data = values.data;
        } else if (this.data && this.data.length !== 0) {
            param.data = this.data;
        }

        if (values?.id && values?.id.length !== 0) {
            param.id = values?.id;
        }
        if (this.prompt.length !== 0) {
            param.prompt = this.prompt;
        }
        if (this.placeholder.length !== 0) {
            param.placeholder = this.placeholder;
        }
        if (this.class.length !== 0) {
            param.class = [];
            this.class.forEach(clazz => param.class.push(clazz));
        }
        if (this.constraints) {
            param.constraints = this.constraints.toHamParamConstraints();
        }
        if (this.selectables.length !== 0) {
            param.options = {}
            param.options.selectables = [];
            this.selectables.forEach(selectable => param.options.selectables.push(selectable.toHamParamSelectables()));
        }

        return param;
    }

    toHiddenHamParam(data = "") {
        if (data && typeof data === 'string' && data.length !== 0) {
            return {name: this.name, type: "hidden", data : data};
        } else if (this.data && this.data.length !== 0) {
            return {name: this.name, type: "hidden", data : this.data};
        }
        return {name: this.name, type: "hidden", data : ""};
    }
}

class Constraints {

    constructor(constraints = {required : false, maxItems : 1, min : "", max : "", regex : "", readonly : false, step : "", accept : ""}) {
        this.required = constraints?.required ?? false;
        this.maxItems = constraints?.maxItems ?? 1;
        this.min = constraints?.min ?? "";
        this.max = constraints?.max ?? "";
        this.regex = constraints?.regex ?? "";
        this.readonly = constraints?.readonly ?? false;
        this.step = constraints?.step ?? "";
        this.accept = constraints?.accept ?? "";
    }

    clone() {
        return new Constraints({required : this.required, maxItems : this.maxItems, min : this.min, max : this.max, regex : this.regex, readonly : this.readonly, step : this.step, accept : this.accept})
    }

    withRequired(required = false) {
        let newConst = this.clone();
        newConst.required = required ?? false;
        return newConst;
    }
    withMaxItems(maxItems = 1) {
        let newConst = this.clone();
        newConst.maxItems = maxItems ?? 1;
        return newConst;
    }
    withMin(min = "") {
        let newConst = this.clone();
        newConst.min = min ?? "";
        return newConst;
    }
    withMax(max = "") {
        let newConst = this.clone();
        newConst.max = max ?? "";
        return newConst;
    }
    withRegex(regex = "") {
        let newConst = this.clone();
        newConst.regex = regex ?? "";
        return newConst;
    }
    withReadonly(readonly = false) {
        let newConst = this.clone();
        newConst.readonly = readonly ?? false;
        return newConst;
    }
    withStep(step = "") {
        let newConst = this.clone();
        newConst.step = step ?? "";
        return newConst;
    }
    withAccept(accept = "") {
        let newConst = this.clone();
        newConst.accept = accept ?? "";
        return newConst;
    }

    toHamParamConstraints() {
        let constraints = {};

        if (this.required) {
            constraints.minItems = 1;
        } else {
            constraints.minItems = 0;
        }
        if (this.min.length !== 0) {
            constraints.min = this.min;
        }
        if (this.max.length !== 0) {
            constraints.max = this.max;
        }
        if (this.regex.length !== 0) {
            constraints.regex = this.regex;
        }
        if (this.readonly) {
            constraints.readonly = true;
        }
        if (this.step.length !== 0) {
            constraints.step = this.step;
        }
        if (this.accept.length !== 0) {
            constraints.accept = this.accept;
        }

        return constraints;
    }
}

class Selectable {
    constructor(selectable = {data : "", prompt : "", href : "", selected : false, id : "", selectables : []}) {
        if ((!selectable?.data && (!selectable?.selectables || selectable.selectables.length === 0)) 
                || ((selectable?.data && selectable.data.length !== 0) && (selectable?.selectables && selectable.selectables.length !== 0))) {
            throw "Either data or selectables must be set";
        }
        this.data = selectable?.data ?? "";
        this.prompt = selectable?.prompt ?? "";
        this.href = selectable?.href ?? "";
        this.id = selectable?.id ?? "";
        this.selected = selectable?.selected ?? false;
        this.selectables = selectable?.selectables ?? [];
    }

    clone() {
        return new Selectable({data : this.data, prompt : this.prompt, href : this.href, selected : this.selected, id : this.id, selectables : Selectable.cloneArray(this.selectables)})
    }

    static cloneArray(selectables) {
        return [...selectables];
    }

    withData(data = "") {
        let newSelectable = this.clone();
        newSelectable.data = data ?? "";
        return newSelectable;
    }
    withPrompt(prompt = "") {
        let newSelectable = this.clone();
        newSelectable.prompt = prompt ?? "";
        return newSelectable;
    }
    withHref(href = "") {
        let newSelectable = this.clone();
        newSelectable.href = href ?? "";
        return newSelectable;
    }
    withSelected(selected = false) {
        let newSelectable = this.clone();
        newSelectable.selected = selected ?? false;
        return newSelectable;
    }
    withId(id = "") {
        let newSelectable = this.clone();
        newSelectable.id = id ?? "";
        return newSelectable;
    }
    withSelectables(selectables = []) {
        let newSelectable = this.clone();
        newSelectable.selectables = selectables ?? [];
        return newSelectable;
    }

    toHamParamSelectables() {
        let selectable = {}

        if (this.data.length !== 0) {
            selectable.data = this.data;
        }
        if (this.prompt.length !== 0) {
            selectable.prompt = this.prompt;
        }
        if (this.href.length !== 0) {
            selectable.href = this.href;
        }
        if (this.id.length !== 0) {
            selectable.id = this.id;
        }
        if (this.selected) {
            selectable.selected = true;
        }
        if (this.selectables.length != 0) {
            selectable.selectables = this.selectables.toHamParamSelectables();
        }

        return selectable;
    }
}

class Steps {

    templatesStepMap = new Map();
    commonItemsStepMap = new Map();

    constructor(commonParamTypes = []) {
        this.validateParamType(commonParamTypes);
        this.commonParamTypes = commonParamTypes ?? [];
    }

    addParamTypesForStep(step, paramTypes) {
        if (!step || !(typeof step === 'string')) {
            throw "step must be provided and of instance of String";
        }
        this.validateParamType(paramTypes);

        // No duplicated types please. Last one wins.
        let paramTypesMap = new Map();
        paramTypesMap.set("_current-step", new ParamType("_current-step", {data: step, type: "hidden"}));
        paramTypes.forEach(paramType => paramTypesMap.set(paramType.name, paramType));
        this.templatesStepMap.set(step, paramTypesMap.values());
    }

    addCommonItemsForStep(step, items = []) {
        if (!step || !(typeof step === 'string')) {
            throw "step must be provided and of instance of String";
        }
        this.validateItem(items);
        this.commonItemsStepMap.set(step, items);
    }

    paramTypesForStep(step, common = true, current = true) {
        if (!this.templatesStepMap.has(step)) {
            return [];
        }
        let givenEntries = this.templatesStepMap.get(step);
        let entries = [...givenEntries];
        if (common) {
            this.commonParamTypes.forEach(paramType => entries.push(paramType));
        }
        return entries;
    }

    allStepNames() {
        return [...this.templatesStepMap.keys()]
    }

    paramTypeByName(paramTypeName) {

        let stepNames = this.allStepNames();
        for (var i = 0; i < stepNames.length; i++) {
            let paramTypes = this.paramTypesForStep(stepNames[i]);
            for (var j = 0; j < paramTypes.length; j++) {
                if (paramTypes[j].name === paramTypeName) {
                    return paramTypes[j];
                }
            }
        }
        return null;
    }

    commonItemsForStep(step) {
        if (!this.templatesStepMap.has(step)) {
            return [];
        }
        return [...this.commonItemsStepMap.get(step)];
    }

    validateParamType(paramTypes = []) {
        if (!Array.isArray(paramTypes)) {
            throw "paramTypes must by an array";
        }
        paramTypes.forEach((type, index) => {
            if (!(type instanceof ParamType)) {
                throw "paramType@" + index + " is not of type ParamType";
            }
        });
    }

    validateItem(items = []) {
        if (!Array.isArray(items)) {
            throw "items must by an array";
        }
        items.forEach((item, index) => {
            if (!(item instanceof Item)) {
                throw "item@" + index + " is not of type Item";
            }
        });
    }
}

class Link {
    constructor(rel, href, additional = {id: "", name: "", prompt: "", transclude: false}) {
        this.rel = this.validateRel(rel);
        this.href = this.validateHref(href);
        this.name = additional?.name ?? "";
        this.prompt = additional?.prompt ?? "";
        this.id = additional?.id ?? "";
        this.transclude = additional?.transclude ?? false;
    }

    clone() {
        return new Link(this.rel, this.href, {id: this.id, name: this.name, prompt: this.prompt, transclude: this.transclude})
    }

    validateHref(href) {
        if (!href || href.length === 0) {
            throw "href must be provided";
        }
        return href;
    }

    validateRel(rel) {
        if (!rel || rel.length === 0) {
            throw "Rel must be provided";
        }
        if (rel.includes(" ") || rel.includes("\t")) {
            throw "Rel must not contain a whitespace";
        }
        return rel;
    }

    withRel(rel) {
        let newLink = this.clone();
        newLink.rel = this.validateRel(rel);
        return newLink;
    }
    withHref(href) {
        let newLink = this.clone();
        newLink.href = this.validateHref(href);
        return newLink;
    }
    withId(id = "") {
        let newLink = this.clone();
        newLink.id = id ?? "";
        return newLink;
    }
    withName(name = "") {
        let newLink = this.clone();
        newLink.name = name ?? "";
        return newLink;
    }
    withPrompt(prompt = "") {
        let newLink = this.clone();
        newLink.prompt = prompt ?? "";
        return newLink;
    }
    withTransclude(transclude = false) {
        let newLink = this.clone();
        newLink.transclude = transclude ?? false;
        return newLink;
    }

    toHamLink() {
        let hamLink = {rel: this.rel, href: this.href};

        if (this.name.length !== 0) {
            hamLink.name = this.name;
        }
        if (this.prompt.length !== 0) {
            hamLink.prompt = this.prompt;
        }
        if (this.id.length != 0) {
            hamLink.id = this.id;
        }
        if (this.transclude) {
            hamLink.transclude = true;
        }

        return hamLink;
    }
}

class Entry {
    constructor(name, additional = {value: null, prompt: "", id: "", entries: []}) {
        this.name = this.validateName(name);
        this.prompt = additional?.prompt ?? "";
        this.id = additional?.id ?? "";
        this.value = additional?.value;
        this.entries = additional?.entries ?? [];
    }

    clone() {
        return new Entry(this.name, {prompt: this.prompt, value: this.value, id: this.id, entries: [...this.entries]});
    }

    validateName(name) {
        if (!name || name.length === 0) {
            throw "name must be provided";
        }
        return name;
    }

    withName(name) {
        let newLink = this.clone();
        newLink.name = nvalidateName(name);
        return newLink;
    }
    withPrompt(prompt = "") {
        let newLink = this.clone();
        newLink.prompt = prompt ?? "";
        return newLink;
    }
    withId(id = "") {
        let newLink = this.clone();
        newLink.id = id ?? "";
        return newLink;
    }
    withValue(value) {
        let newLink = this.clone();
        newLink.value = value;
        return newLink;
    }
    withEntries(entries = []) {
        let newLink = this.clone();
        newLink.entries = entries ?? [];
        return newLink;
    }

    toHamEntry() {
        let hamEntry = {name: this.name};

        if (this.prompt.length !== 0) {
            hamEntry.prompt = this.prompt;
        }
        if (this.id.length !== 0) {
            hamEntry.id = this.id;
        }
        if (this.entries.length !== 0) {
            hamEntry.entries = [...this.entries];
        }
        
        hamEntry.value = this.value;

        return hamEntry;
    }
}

class Item {
    constructor(data = {id: "", links: [], entries: []}) {
        this.id = data?.id ?? "";
        this.links = data?.links ?? [];
        this.entries = data?.entries ?? [];
    }

    clone() {
        return new Item({id: this.id, links: [...this.links], entries: [...this.entries]});
    }

    withId(id = "") {
        let newItem = clone();
        newItem.id = id ?? "";
        return newItem;
    }

    containsLinksOrEntries() {
        return this.links.length !== 0 || this.entries.length !== 0;
    }

    // Yet we don't check for duplicated entry names and not for duplicated link (rel|href) tuple.
    withEntries(entries = []) {
        let newItem = this.clone();
        newItem.entries = entries ?? [];
        return newItem;
    }
    withLinks(links = []) {
        let newItem = this.clone();
        newItem.links = links ?? [];
        return newItem;
    }
    addLinks(links = []) {
        if (!links || links.length === 0) {
            return this;
        }
        let newItem = this.clone();
        links.forEach(link => newItem.links.push(link))
        return newItem;
    }
    addEntries(entries = []) {
        if (!entries || entries.length === 0) {
            return this;
        }
        let newItem = this.clone();
        entries.forEach(entry => newItem.entries.push(entry))
        return newItem;
    }

    toHamItem() {
        let hamItem = {};

        if (this.id.length !== 0) {
            hamItem.id = this.id;
        }
        if (this.links.length !== 0) {
            hamItem.links = [];
            this.links.forEach(link => hamItem.links.push(link.toHamLink()));
        }
        if (this.entries.length !== 0) {
            hamItem.entries = [];
            this.entries.forEach(entry => hamItem.entries.push(entry.toHamEntry()));
        }

        return hamItem;
    }
}

function selectablesForKrankenkassen() {

    let names = [];
    KRANKENKASSEN.forEach(entry => {
        entry.names.forEach(name => names.push(name));
    });
    names.sort();
    let selectables = [];
    names.forEach(name => selectables.push(new Selectable({data: name})));

    return selectables;
}

const KRANKENKASSEN = [
    {
        group: "Allgemeine Ortskrankenkassen",
        names: [
            "AOK Baden-Württemberg", "AOK Bayern", "AOK Bremen/Bremerhaven", "AOK Hessen", "AOK Niedersachsen", "AOK Nordost", "AOK Nordwest", "AOK Plus", "AOK Rheinland/Hamburg", "AOK Rheinland-Pfalz/Saarland", "AOK Sachsen-Anhalt"
        ]
    },
    {
        group: "Ersatzkassen",
        names: [
            "Barmer Ersatzkasse", "DAK-Gesundheit", "Handelskrankenkasse", "HEK – Hanseatische Krankenkasse", "Kaufmännische Krankenkasse – KKH", "Techniker Krankenkasse"
        ]
    },
    {
        group: "Innungskrankenkassen",
        names: [
            "Bundesinnungskrankenkasse Gesundheit", "IKK classic", "IKK gesund plus", "IKK Südwest", "IKK – Die Innovationskasse", "Innungskrankenkasse Brandenburg und Berlin"
        ]
    },
    {
        group: "Betriebskrankenkassen",
        names: [
            "Audi BKK", "Bahn-BKK",
            "Bertelsmann BKK",
            "Betriebskrankenkasse BPW Bergische Achsen KG",
            "Betriebskrankenkasse der BMW AG",
            "Betriebskrankenkasse der G. M. Pfaff AG Kaiserslautern",
            "Betriebskrankenkasse EVM",
            "Betriebskrankenkasse Firmus",
            "Betriebskrankenkasse Groz-Beckert",
            "Betriebskrankenkasse Mahle",
            "Betriebskrankenkasse Miele",
            "Betriebskrankenkasse Mobil",
            "Betriebskrankenkasse Schwarzwald-Baar-Heuberg",
            "Betriebskrankenkasse Vereinigte Deutsche Nickel-Werke",
            "Betriebskrankenkasse WMF Württembergische Metallwarenfabrik AG",
            "BKK24",
            "BKK B. Braun Aesculap",
            "BKK Akzo Nobel Bayern",
            "BKK Deutsche Bank AG",
            "BKK Diakonie",
            "BKK Dürkopp Adler",
            "BKK Euregio",
            "BKK EWE",
            "BKK exklusiv",
            "BKK Faber-Castell & Partner",
            "BKK Freudenberg",
            "BKK Gildemeister Seidensticker",
            "BKK Herkules",
            "BKK Linde",
            "BKK Melitta HMR",
            "BKK MTU",
            "BKK Pfalz",
            "BKK Provita",
            "BKK Public",
            "BKK PwC",
            "BKK Rieker Ricosta Weisser",
            "BKK Salzgitter",
            "BKK Scheufelen",
            "BKK Stadt Augsburg",
            "BKK Technoform",
            "BKK VBU",
            "BKK Verbundplus",
            "BKK Voralb Heller Index Leuze",
            "BKK Werra-Meissner",
            "BKK Wirtschaft & Finanzen",
            "BKK Würth",
            "BKK ZF & Partner",
            "Bosch BKK",
            "Continentale Betriebskrankenkasse",
            "Daimler Betriebskrankenkasse",
            "Debeka BKK",
            "Die Bergische Krankenkasse",
            "Energie-Betriebskrankenkasse",
            "Ernst & Young BKK",
            "Gemeinsame Betriebskrankenkasse der Gesellschaften der Textilgruppe Hof",
            "Heimat Krankenkasse",
            "Karl Mayer Betriebskrankenkasse",
            "Koenig & Bauer BKK",
            "Krones Betriebskrankenkasse",
            "Merck BKK",
            "Mhplus Betriebskrankenkasse",
            "Novitas BKK",
            "Pronova BKK",
            "R+V Betriebskrankenkasse",
            "Salus BKK",
            "Securvita BKK",
            "Siemens-Betriebskrankenkasse",
            "SKD BKK",
            "Südzucker BKK",
            "TUI BKK",
            "Viactiv BKK",
            "Vivida BKK"
        ]
    },
    {
        group: "Deutsche Rentenversicherung Knappschaft-Bahn-See",
        names: [
            "Knappschaft"
        ]
    },
    {
        group: "Sozialversicherung für Landwirtschaft, Forsten und Gartenbau",
        names: [
            "Landwirtschaftliche Krankenkasse"
        ]
    }
];
